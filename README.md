# ÖRDO

## Composizione del Team:
- Alessandra La Mura
- Alvaro Penas Guevara
- Anthony Stheven Luna Gonzales
- Dario Ciaudano
- Luca Marinuzzi


## Cos'è ÖRDO
ÖRDO è un ecosistema digitale, il quale viene incontro alla necessità delle aziende, come Fameccanica, di entrare nell’industria 4.0. Diventa quindi decisiva la fusione dei propri dati grezzi con l’arricchimento dall’ecosistema esterno al fine di migliorare, monitorare e prevedere malfunzionamenti o guasti lungo tutta la produzione.

Così come nel 1780 con il primo telaio a vapore avveniva la prima rivoluzione industriale, oggi stiamo assistendo alla quarta rivoluzione, e il servizio che offriamo non offre solo un prodotto, ma la fa in maniera innovativa e sostenibile.

Questa nostra innovazione è vicina ai termini dello sviluppo sostenibile, quindi vuol dire produrre in maniera equa e dignitosa per tutti, senza sfruttare - fino a depauperare - i sistemi naturali da cui traiamo risorse e senza oltrepassare le loro capacità di assorbire scarti e rifiuti, generati dalle nostre attività. Punto cardine del nostro modello è la creazione di un team diversificato e inclusivo per favorire l'innovazione.

Mostriamo così il nostro punto di forza, con anche tutti i nostri key partner, poiché vogliamo portare allo sviluppo di un ecosistema si digitale, ma in armonia e rispetto con il pianeta e con i bisogni di tutte le persone.

La nostra proposta prevede l'utilizzo di modelli statistici utilizzati da AI, intelligenza artificiale, al fine di apprendere e migliorare le performance di uno stabilimento produttivo, e nel caso di un cliente con diversi stabilimenti sarà in grado di unire sotto un'unico modello matematico le varie produzioni.
Al fine di ottenere un incremento nella produzione:
- Iniziale preprocessamento dei dai, i quali vengono inseriti in una coda di evasione per venir computati in base a diverse metriche
- Continuo aggiornamento del modello con i dati forniti a nastro continuo dai vari sensori, questo implica una maggiore sensibilità del sistema ogni volta che immagazzina nuova dati.
- Analisi statistiche date dall'interpolazione di dati e regressione degli stessi al fine di catturare tutti gli elementi outsider ed analizazrli per comprendere se siano dovuti a un malfunzionamento del macchinario o a un guasto lungo la produzione.
- I dati dell'analisi proposta prima sono usati anche per gestire un modello di regressione di deep learning al fine di trovare data una finestra temporale un malfunzionamento al suo interno.
- Analisi mirate nel dominio di settore relativo al di riferimento mercato di produzione del cliente a cui offriamo il nostro servizio. 

Il nostro prodotto non solo da una visione interna alla propria attività, ma analizza anche i fattori esterni ad essa per poter suggerire delle strategie economiche vantaggiose, avendo come fine ultimo l'ottimizzazione del processo di produzione.

Una volta esposto il core del nostro progetto, ci teniamo a precisare che l'utente che andrà ad usare l'ecosistema digitale, potrà interagire con esso nella maniera che preferisce. Quindi avrà a disposizione una visualizzazione più semplice e meno tecnica, adatta a chiunque persona, ed un'altra più tecnica dove sono presenti statistiche e metriche proprie di un tecnico del settore.


## Con quale modello strategico?
![business model canvas presentazione](canvas.jpg?raw=true)

Nel seguente documento, [business model canvas](https://gitlab.com/DanoSan/ordo/-/blob/main/Business_Model_Canvas_nel_dettaglio_.pdf), viene presentato il modello di business.

## Quali sono i possibili rischi/benefici di questo sistema?
![analisi swot presentazione](swot.jpg?raw=true)

Nel seguente documento, [analisi SWOT](https://gitlab.com/DanoSan/ordo/-/blob/main/Analisi_SWOT_nel_Dettaglio.pdf), vengono presentati i vari punti di forza e di debolezza con le relative soluzioni studiare per arginarne gli effetti negativi.


## Analisi Tecnica
Nel seguente documento, [relazione tecnica](https://gitlab.com/DanoSan/ordo/-/blob/main/Relazione_tecnica.pdf), viene presentata l'architettura del software.

## DEMO
Abbiamo realizzato una breve [demo pratica](https://www.figma.com/proto/zxx964d77hSSjlsHR7mnCW/ORDO-Demo?page-id=0%3A1&node-id=4856%3A178&viewport=241%2C48%2C0.09&scaling=min-zoom&starting-point-node-id=4856%3A178) al fine mi aiutare la comprensione della nostra soluzione innovativa.